### Bitácora

| Versión        | Cambio Realizdo            | Autor del cambio | Aprobado por   | Fecha de cambio |
| -------------  | --------                   | -------          | -------------  | -------------   |
| 1.0.0          | Despliegue versión inicial | Karina           | Bruno          | 06/09/2018      |