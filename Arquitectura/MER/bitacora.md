﻿### Bitácora

| Versión        | Cambio Realizdo     | Autor del cambio | Aprobado por   | Fecha de cambio |
| -------------  | --------            | -------          | -------------  | -------------   |
| 1.0.0          | MER versión inicial | Brenda           | Karina         | 07/09/2018      |
| 1.15.0         | MER con atributos   | Romel            | Bruno          | 02/10/2018      |