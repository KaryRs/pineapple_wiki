### Bitácora

| Versión        | Cambio Realizdo      | Autor del cambio | Aprobado por   | Fecha de cambio |
| -------------  | --------             | -------          | -------------  | -------------   |
| 1.0.0          | Stack versión inicial | Bruno            | Ivett               | 06/09/2018      |